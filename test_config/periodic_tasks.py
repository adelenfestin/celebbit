from celery.task import task
import time

@task
def time_ticker():
    print "Time now is " + time.strftime("%b %d %Y %H:%M:%S")


@task
def say_hi(name):
    print "Hi " + name + "!"