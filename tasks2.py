import time
from celery import Celery

# app = Celery('tasks', backend='amqp', broker='amqp://')

@app.task
def hello_with_a_tag(tag):
    print "Hello! I am message #" + str(tag)

@app.task
def get_summation(n):
    print "Computing summation from 1 to " + str(n)
    sum = 0
    for i in xrange(1, n+1):
        sum += i
    return sum

@app.task
def exception_raiser(n):
    time.sleep(5)
    raise ValueError('An exception was raised!!!')
    return "WOKE UP!"