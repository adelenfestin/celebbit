from celery.task import task

@task
def square(x):
    product = x * x
    print "[SQUARE] I GOT " + str(product)
    return "[SQUARE] RESULT: " + str(product)


