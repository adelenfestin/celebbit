from __future__ import absolute_import

import os
import celery
from datetime import timedelta

CELERY_IGNORE_RESULT = False

broker_username = os.environ.get('BROKER_USERNAME')
broker_password = os.environ.get('BROKER_PASSWORD')
broker_host = os.environ.get('BROKER_HOST')
broker_port = os.environ.get('BROKER_PORT')

if broker_username and broker_password and broker_host and broker_port:
  BROKER_URL= "".join(["amqp://", 
                       broker_username, 
                       ":", 
                       broker_password, 
                       "@", 
                       broker_host, 
                       ":", 
                       broker_port])
else:
  BROKER_URL= "amqp://"

print BROKER_URL

CELERY_RESULT_BACKEND = "amqp"

CELERY_IMPORTS=("tasks", "tasks2", "periodic_tasks")


from celery.schedules import crontab

CELERYBEAT_SCHEDULE = {
  'say-hi-every-10-seconds': {
    'task': 'periodic_tasks.say_hi',
    'schedule': timedelta(seconds=30),
    'args': ['user']
  },

  'tell-time-every-minute': {
    'task': 'periodic_tasks.time_ticker',
    'schedule': crontab(minute='*/1')
  }
}

