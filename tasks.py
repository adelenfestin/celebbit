import time
from celery import Celery

# Default broker: amqp://guest:**@localhost:5672//
app = Celery('tasks', backend='amqp', broker='amqp://guest@localhost:5672//')

@app.task
def hello():
    print "Hello from the other sideee ~"