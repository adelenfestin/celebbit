from celery.task import task

@task
def double(x):
    product = 2 * x
    print "[DOUBLE] I GOT " + str(product)
    return "[DOUBLE] RESULT: " + str(product)


